'use strict';

var stompClient = null;
var username = "SYLVAIN";
var intervalID;


function connect() {
	var socket = new SockJS('/ws');
	stompClient = Stomp.over(socket);
	stompClient.debug = null;
	stompClient.connect({}, onConnected, onError);
}


function onConnected() {
	
	// Subscribe to the Public Topic
	stompClient.subscribe('/topic/public', onMessageReceived);

	// Tell your username to the server

	var message = {
			sender: username,
			content: "Connection",
			type: 'JOIN'
	};

	stompClient.send("/app/chat.addUser", {}, JSON.stringify(message));
	console.log("ENVOI MSG : "+JSON.stringify(message));
}


function onError(error) {
	console.error("ERROR : "+error);
	setTimeout(connect, 10000);
}


function sendMessage(msg) {
	if(msg && stompClient) {
		var message = {
				sender: username,
				content: msg,
				type: 'CHAT'
		};

		stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(message));
		console.log("ENVOI MSG : "+JSON.stringify(message));
	}
}


function onMessageReceived(payload) {
	var message = JSON.parse(payload.body);
	console.log("RECEPTION : "+JSON.stringify(message));
	var toastHTML = '<span>I am a websocket event : </span>'+message.content+'</button>';
	M.toast({html: toastHTML, classes: 'rounded'});
}


