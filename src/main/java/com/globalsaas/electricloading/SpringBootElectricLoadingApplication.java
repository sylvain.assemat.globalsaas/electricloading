package com.globalsaas.electricloading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootElectricLoadingApplication {

	private final static Logger LOGGER = LoggerFactory.getLogger(SpringBootElectricLoadingApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootElectricLoadingApplication.class, args);
		
		LOGGER.info("os.name    : "+System.getProperty("os.name"));
		LOGGER.info("os.arch    : "+System.getProperty("os.arch"));
		LOGGER.info("os.version : "+System.getProperty("os.version"));
	}

}
