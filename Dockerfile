FROM hypriot/rpi-java
ADD target/electric-loading-0.0.1-SNAPSHOT.jar /opt/electric-loading-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=\"prod\"", "/opt/electric-loading-0.0.1-SNAPSHOT.jar"]